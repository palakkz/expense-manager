const http=require("http");
const mongoose=require("mongoose");
const express=require("express");
const bodyParser=require("body-parser");
var app=express();
const https=require("https");
const fs=require("fs");
const csrf=require("csurf");
const path=require("path");
const flash=require("connect-flash");
const authRoutes=require("./routes/auth");
const userRoutes=require("./routes/user");
const session=require("express-session");
const errorController=require("./controllers/error");
const User=require("./models/user");
const methodOverride=require("method-override");
const MongodbURI="mongodb+srv://user1:palakkhatri@cluster0-ar7ck.mongodb.net/assignment?retryWrites=true&w=majority";
const MongoDBStore=require("connect-mongodb-session")(session);
const store=new MongoDBStore({
    uri: MongodbURI,
    collection:'sessions'
});
app.use(session({secret:'my secret',save:false,saveUninitialized:false,store:store}));
app.use(methodOverride("_method"));
app.use(bodyParser.json());

app.use(flash());

const csrfProtection=csrf();
app.set("view engine","ejs");

app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,"public")));

app.use(csrfProtection);

app.use((req,res,next)=>{
    res.locals.isAuthenticated=req.session.isLoggedIn;
    res.locals.csrfToken=req.csrfToken();
    next();
 });
 
app.use((req,res,next)=>{
    
    if(!req.session.user){
        
        return next();
    }
    User.findById(req.session.user._id)
    .then(user=>{
            if(!user){
                 next();
            }
            else{
                req.user =user;
                next();
            }
    })
    .catch(err=>{
        next(new Error(err));
    });
    
 });
 app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
  });

app.use(authRoutes);
app.use(userRoutes);
app.get('/500', errorController.get500);
app.use(errorController.get404);
app.use((error,req,res,next)=>{
    console.log(error);
    res.status(500).render("500",{
        pageTitle:"Error!!",
        path:"/500",
        isAuthenticated:req.session.isLoggedIn
    });
});
mongoose.connect(MongodbURI)
 . then(result=>{
    
    app.listen(process.env.PORT || 3000);
 })
 .catch(err=>{
     console.log(err);
 })