const path=require("path");
const fs=require("fs");
const User=require("../models/user");
const Account=require("../models/account");
const Transaction=require("../models/transaction");
const {validationResult}=require("express-validator/check");
exports.getIndex=(req,res,next)=>{
    res.render("user/index",{
        pageTitle:"Home",
        path:"/"
    });
};
exports.getHomepage=(req,res,next)=>{
    Account.find({ownerId:req.userId})
    .then(accounts=>{
        
        res.render("user/homepage",{
            path:"/homepage",
            pageTitle:"Home",
            accounts:accounts,
        });
    })
    .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
   });
    

}

exports.getAccount=(req,res,next)=>{
    
    Account.findById(req.params.account_id)
    .then(account=>{
        res.render("user/account",{
            pageTitle:"Account",
            account:account,
            accountId:req.params.account_id,
            path:"/account/:account_id",
            errorMessage:req.flash('error'),
        });

    })
    .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    
    })
    
    
}
exports.getAddAccount=(req,res,next)=>{
    
    
        res.render("user/add-account",{
            pageTitle:"Add Account",
            path:"/add-account",
            errorMessage:req.flash('error'),
            userId:req.user
        
    });
    
};
exports.postAddAccount=(req,res,next)=>{
        User.findById(req.userId)
        .then(user=>{
        
            const account=new Account({
                name:req.body.name,
                ownerId:user._id
            })
           
            account.save();
            user.account.push(account._id);
            user.save()
            .then(result=>{
                console.log(result);
                console.log("Created Account");
            })
            .catch(err=>{
                const error=new Error(err);
                error.httpStatusCode = 500;
                return next(error);
            
            });
        })
        res.redirect("/homepage");
}
exports.getEditAccount=(req,res,next)=>{
   
    const acc_id=req.params.account_id;
    Account.findById(acc_id)
    .then(account=>{
    
        if(!account){
            return res.redirect("/homepage");
        }
        res.render("user/edit-account",{
            pageTitle:"Edit Account",
            path:"/edit-account",
            account:account,
            errorMessage:null,
            validationErrors:[]

        });
    }).catch(err=>
        {  
            const error=new Error(err);
            error.httpStatusCode = 500;
            return next(error);
          
        });
}
exports.postEditAccount=(req,res,next)=>{

    const acc_id=req.body.accountId;
    const updatedName=req.body.name;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        console.log(errors.array());
      return res.status(422).render('user/edit-account', {
        pageTitle: 'Edit Account',
        path: '/edit-account',
        editing: true,
        hasError: true,
        account: {
          name: updatedName,
          _id: acc_id
        },
        errorMessage: errors.array()[0].msg,
        validationErrors: errors.array()
      });
    }
   
   Account.findById(acc_id).then(account=>{
       
      
       if(account.ownerId.toString() != req.userId.toString()){
             return res.redirect("/");
        }
       account.name=updatedName;
       
       return account.save().then(result=>{
        res.redirect("/homepage");
        console.log("UPDATED RECORD");
        });
})
    .catch(err=> { const error=new Error(err);
    error.httpStatusCode = 500;
    return next(error);
    });
}
exports.DeleteAccount=(req,res,next)=>{
    const acc_id=req.body.accountId;
    
    Account.findByIdAndDelete(acc_id)
    .then(account=>{
        if(!account){
            return next(new Error('Account not found'));
        }
       else{
        return Account.deleteOne({_id:acc_id,userId:req.userId})
       }
    })
    .then(result=>{
            console.log("DESTROYED account");
            res.redirect("/homepage");
        })
        .catch(err=>{
            res.status(500).json({message:"Deleting product failed."});
          
        });
   
}
exports.getAddUser=(req,res,next)=>{
    const user=req.user;
    Account.findById(req.params.account_id).populate('users')
    .then(account=>{
        User.find({})
        .then(users=>{
        
        res.render("user/add-user",{
            path:"/account/:account_id/add-user",
            pageTitle:"Add User",
            userId:req.userId,
            users:users,
            user:req.user,
            account:account,
            isAuthenticated: req.session.isLoggedIn,
            errorMessage:req.flash('error')
        });
    })
    .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      
    })
});
}
exports.postAddUser=(req,res,next)=>{
    const email=req.body.email;
    User.findOne({email:req.body.email})
    .then(user=>{
       
        Account.findByIdAndUpdate(req.params.account_id).populate('users')
        .then(account=>{
            if(account.users.length>0){
                for(var i=0;i<account.users.length;i++){
                    if(account.users[i]._id.toString()===user._id.toString()){
                        return next(new Error('User is already added'));
                    }else{
                        account.users.push(user._id);
                        account.save();
                    }
                }
            }else{
                account.users.push(user._id);
                account.save();
            }
            res.redirect("/homepage");
        })
    })
    .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    })
}
exports.getUsers=(req,res,next)=>{
    Account.findById(req.params.account_id).populate('users')
    .then(account=>{
        User.findById(account.users)
        .then(user=>{
            
            res.render("user/users",{
                pageTitle:"Users",
                path:"/account/:account_id/users",
                user:user,
                currentUser:req.user,
                account:account,
                errorMessage:null,
                validationErrors:[]
    
            });
        })
        
    })
}
exports.getAddTransaction=(req,res,next)=>{
    
        User.findById(req.userId).populate('account')
        .then(user=>{
            
        Account.findById(user.account)
        .then(account=>{
        
            res.render("user/add-transaction",{
            
                pageTitle:"Add Transaction",
                userId:req.user,
                path:"/add-transaction",
                user:user,
                account:account,
                isAuthenticated: req.session.isLoggedIn,
                errorMessage:req.flash('error')
        })
        })
    })
    .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      
    })
}
exports.postAddTransaction=(req,res,next)=>{
    const account=req.body.account;
    Account.findOne({name:account}).populate('transactions')
    .then(account=>{
        
        const transaction=new Transaction({
            account:req.body.account,
            group:req.body.type,
            category:req.body.category,
            amount:req.body.amount
        });
        transaction.save();
        
        if(account.ownerId.toString()===req.userId.toString()){
            
            account.transactions.push(transaction._id);
        }
        account.save() 
        
        .then(result=>{
            
            console.log("Created Transaction");
        })
        .catch(err=>{
            const error=new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        
        });
    })
    res.redirect("/homepage");
    
}
exports.postTransactions=(req,res,next)=>{
    const type=req.body.type;
    Account.findById(req.params.account_id).populate('transactions')
    .then(account=>{
     
    Transaction.findById(account.transactions)
    .sort({created:-1})
    .then(transaction=>{
        res.render("user/transactions",{
                pageTitle:"Transactions",
                    userId:req.userId,
                    path:"/account/:account_id/transactions",
                    transaction:transaction,
                    account:account,
                    type:type,
                    isAuthenticated: req.session.isLoggedIn,
                    errorMessage:req.flash('error')
            })
    })
    .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    
    });

    })
    
}

exports.getEditTransaction=(req,res,next)=>{
    
        User.findById(req.userId).populate('account')
        .then(user=>{
            
        Account.findById(user.account)
        .then(account=>{
            Transaction.findById(req.params.transaction_id)
            .then(transaction=>{
                
            res.render("user/edit-transaction",{
                pageTitle:"Edit Transaction",
                path:"/transaction/:transaction_id/edit-transaction",
                errorMessage:req.flash('error'),
                user:user,
                account:account,
                transaction:transaction
            })

        })
        .catch(err=>{
            const error=new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        
        });
    })
    })
    
}

exports.postEditTransaction=(req,res,next)=>{
    const trans_id=req.body.transactionId;
    const updatedAccount=req.body.account;
    const updatedType=req.body.type;
    const updatedCategory=req.body.category;
    const updatedAmount=req.body.amount;
   
   Account.findOne({name:updatedAccount}).populate('transactions')
   .then(account=>{
    Transaction.findByIdAndUpdate(trans_id).then(transaction=>{
        transaction.account=updatedAccount,
        transaction.group=updatedType,
        transaction.category=updatedCategory,
        transaction.amount=updatedAmount;
        
        transaction.save()
        if(account.ownerId.toString()===req.userId.toString()){
            
            account.transactions.push(transaction._id);
        }
        return account.save() 
        .then(result=>{
         res.redirect("/homepage");
         console.log("UPDATED RECORD");
         });
     });       

   })
     
}

exports.deleteTransaction=(req,res,next)=>{
    const trans_id=req.body.transactionId;
    
    Transaction.findByIdAndDelete(trans_id)
    .then(transaction=>{
        if(!transaction){
            return next(new Error('Transaction not found'));
        }
       else{
        return Transaction.deleteOne({_id:trans_id,userId:req.userId})
       }
    })
    .then(result=>{
            console.log("DESTROYED TRANSACTION");
            res.redirect("/homepage");
        })
        .catch(err=>{
            res.status(500).json({message:"Deleting transaction failed."});
          
        });
   
}