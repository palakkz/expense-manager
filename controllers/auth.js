const crypto=require('crypto');
const bcrypt=require('bcryptjs');
const nodemailer=require('nodemailer');
const sendgridTransport=require("nodemailer-sendgrid-transport");
const path=require("path");
const {validationResult}=require("express-validator/check");
const User=require("../models/user");
const Account=require("../models/account");
const jwt=require("jsonwebtoken");

const transporter=nodemailer.createTransport(sendgridTransport({
    auth:{
        api_key:'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'
    }
}));
exports.getSignup = (req, res, next) => {
    
    res.render('auth/signup', {
        
      path: '/signup',
      pageTitle: 'SignUp',
      errorMessage:req.flash('error'),
      oldInput:{
        email:'',
        password:'',
        confirmPassword:''
      },
      validationErrors:[]
       
    });
};
exports.postSignup = (req, res, next) => {
  
    const email=req.body.email;
    const password=req.body.password;
    const confirmPassword=req.body.confirmPassword;
    const errors=validationResult(req);
   if(!errors.isEmpty()){
     console.log(errors.array());
     return res.status(422).render('auth/signup', {
          
      path: '/signup',
      pageTitle: 'SignUp',
      errorMessage:errors.array()[0].msg,
      oldInput:{
        email:email,
        password:password,
        confirmPassword:req.body.confirmPassword
      },
       validationErrors:errors.array()
    });
   }
  
      return bcrypt.hash(password,12).then(hashedPassword=>{
        const user=new User({
          email:email,
          password:hashedPassword,
          
        });
        return user.save();
       
        
      })
      .then(result=>{
        
        User.findOne({email:email}).populate('account')
        .then(user=>{
          const account=new Account({
            name:"Default",
            ownerId:user._id
          })
          account.save();
          user.account.push(account._id);
          user.save()
         
        })
        
        
        .then(answer=>{
          res.redirect("/login");
          return transporter.sendMail({
          to:email,
          from:"expense@app-node.com",
          subject:"SignedUpSuccessfully",
          html:"<h1>You successfully signed up!!</h1>"
        });
        })
        
  
      }).catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      
      });
    
};
exports.getLogin=(req,res,next)=>{
    res.render('auth/login', {
      
        path: '/login',
        pageTitle: 'Login',
    
        errorMessage:req.flash('error'),
        oldInput:{
          email:'',
          password:''
        },
        validationErrors:[]
      });
}
exports.postLogin=(req,res,next)=>{
  const email=req.body.email;
  const password=req.body.password;
  const errors=validationResult(req);
  if(!errors.isEmpty()){
    
    return res.status(422).render('auth/login', {
         
     path: '/login',
     pageTitle: 'Login',
     errorMessage:errors.array()[0].msg,
     oldInput:{
       email:email,
       password:password
     },
     validationErrors:errors.array()
    
      
   });
  }
  let loadedUser;
 
  User.findOne({ email: email })
    .then(user => {
      if (!user) {
        return res.status(422).render('auth/login', {
          path: '/login',
          pageTitle: 'Login',
          errorMessage: 'Invalid email or password.',
          oldInput: {
            email: email,
            password: password
          },
          validationErrors: []
        });
      }
      req.session.user=user;
      loadedUser = user;
      bcrypt.compare(password, user.password)
    .then(isEqual => {
      if (!isEqual) {
        const error = new Error('Wrong password!');
        error.statusCode = 401;
        throw error;
      }
      const token=jwt.sign(
        {
          email: loadedUser.email,
          userId: loadedUser._id.toString()
        },
        'somesupersecretsecret',
        { expiresIn: '1h' }
      );
    
      req.session.token = token;
      req.session.isLoggedIn=true; 
      res.redirect("/homepage");
      
    });
  })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });

  
}
exports.getReset=(req,res,next)=>{
    res.render('auth/reset', {
        
        path: '/reset',
        pageTitle: 'Reset Password',
        errorMessage:req.flash('error')
      });
}
exports.postReset=(req,res,next)=>{
    crypto.randomBytes(32,(err,buffer)=>{
      if(err){
        console.log(err);
        res.redirect("/reset");
      }
      const token=buffer.toString('hex');
      User.findOne({email:req.body.email})
      .then(user=>{
        if(!user){
          req.flash('error','No account with that email');
          return res.redirect("/reset");
        }
        user.resetToken=token;
        user.resetTokenExpiration=Date.now()+360000;
        return user.save();
      })
      .then(result=>{
        res.redirect("/");
        transporter.sendMail({
          to:req.body.email,
          from:"expense@app-node.com",
          subject:"Reset Password",
          html:`<p>You have requested for reset password</p>
          <p>Click this <a href="http://localhost:3000/reset/${token}">link</a> to reset password</p>`
        });
  
      })
      .catch(err=>{
        const error=new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      
      });
      
    })
  };
  exports.getNewPassword=(req,res,next)=>{
    const token=req.params.token;
    User.findOne({resetToken:token,resetTokenExpiration:{$gt:Date.now()}})
    .then(user=>{
      
      res.render('auth/newPassword', {
        path: '/newPassword',
        pageTitle: 'New Password',
        errorMessage:req.flash('error'),
        userId:user._id.toString(),
        passwordToken:token
         
      });
    })
    .catch(err=>{
      const error=new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    
    });
  };
  exports.postNewPassword=(req,res,next)=>{
    const newPassword=req.body.password;
    const userId=req.body.userId;
    const passwordToken=req.body.passwordToken;
    let resetUser;
    User.findOne({
      resetToken:passwordToken,
      resetTokenExpiration:{$gt:Date.now()},
      _id:userId
    })
    .then(user=>{
      resetUser=user;
      return bcrypt.hash(newPassword,12);
    })
    .then(hashedPassword=>{
      resetUser.password=hashedPassword;
      resetUser.resetToken=null;
      resetUser.resetTokenExpiration=null;
      return resetUser.save();
  
    })
    .then(result=>{
      res.redirect("/login");
    })
    .catch(err=>{
      const error=new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    
    })
}
exports.postLogout=(req,res,next)=>{
  req.session.destroy(err=>{
    console.log(err);
    res.redirect('/');
  });
}