const mongoose=require("mongoose");
const Schema=mongoose.Schema;
const accountSchema=new Schema({
    name:{
        type:String,
        required:true
    },
    ownerId:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:'User'
    },
    users:[{
        
        type:Schema.Types.ObjectId,
        ref:'User'
    }],
    transactions:[{
        type:Schema.Types.ObjectId,
        ref:'Transaction'
    }]
},
{timestamps:true});
module.exports=mongoose.model('Account',accountSchema);