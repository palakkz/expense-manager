const mongoose=require("mongoose");
const Schema=mongoose.Schema;
const transactionSchema=new Schema({
    account:{
        type:String,
        required:true
    },
    group:{
        type:String,
        required:true
    },
    category:{
        type:String,
        required:true
    },
    amount:{
        type:Number,
        required:true
    },
    created:{
        type:Date,
        default:Date.now
    },
},{timestamps:true});
module.exports=mongoose.model('Transaction',transactionSchema);