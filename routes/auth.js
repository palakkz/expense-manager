const path=require("path");
const express=require("express");
const router=express.Router();
const authController=require("../controllers/auth");
const {check,body} =require("express-validator/check");
const User=require("../models/user");
router.get("/signup",authController.getSignup);
router.get('/reset',authController.getReset);
router.post('/reset',authController.postReset);
router.post('/signup',[check('email').isEmail().withMessage('Please enter valid email.').custom ((value,{req})=>{
   
return User.findOne({email:value})
    .then(userDoc=>{
        if(userDoc){
         return Promise.
         reject('Email is already in use pick another one');
            }
        });
    })
    .normalizeEmail(),
    body('password','Please password of length 8 with only numbers and text')
    .isLength({min: 8})
    .trim(),
    body('confirmPassword').trim().custom((value,{req})=>{
        if(value!==req.body.password){
            throw new Error('Passwords should match!');
        }
        return true;
    })
],authController.postSignup);
router.get("/login",authController.getLogin);
router.post("/login",[
    body('email')
      .isEmail()
      .normalizeEmail()
      .withMessage('Please enter a valid email address.'),
    body('password', 'Password has to be valid.')
      .isLength({ min: 8 })
      .isAlphanumeric()
      .trim()
  ],

  authController.postLogin);
router.get('/reset/:token',authController.getNewPassword);
router.post('/new-password',authController.postNewPassword);
router.get('/logout',authController.postLogout);


module.exports=router;